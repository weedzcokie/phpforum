<?php

function get_parent_categories($category) {
    global $mysqli;
    $parents = array();
    $result = $mysqli->query("SELECT categories_parent AS current, (SELECT categories_name FROM categories WHERE categories_id = current) AS name FROM categories WHERE categories_id = " . $category);
    if ($row = $result->fetch_array()) {
        if ($row['current'] != -1) {
            $parents[] = array('id' => $row['current'],
                'name' => $row['name']);
            $parents = array_merge($parents, get_parent_categories($row['current']));
        }
    }
    return $parents;
}

function format_time($time) {
    $format = 'Y-m-d H:i';
    return date($format, $time);
}

function format_post($content) {
    $content = nl2br($content);
    return $content;
}
function format_post_preview($content) {
    $content = substr($content,0,80);
    return $content;
}

?>
