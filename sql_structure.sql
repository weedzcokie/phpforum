-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: forum
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `announcements_id` int(11) NOT NULL AUTO_INCREMENT,
  `announcements_title` varchar(128) NOT NULL,
  `announcements_description` mediumtext,
  `announcements_content` longtext,
  `announcements_category` int(11) NOT NULL,
  `announcements_topic_id` int(11) NOT NULL,
  `announcements_user_id` int(11) NOT NULL,
  `announcements_time` int(11) DEFAULT NULL,
  `announcements_display` int(1) NOT NULL,
  PRIMARY KEY (`announcements_id`,`announcements_title`,`announcements_category`,`announcements_topic_id`,`announcements_user_id`,`announcements_display`),
  UNIQUE KEY `id_UNIQUE` (`announcements_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `categories_id` int(11) NOT NULL,
  `categories_name` varchar(45) NOT NULL,
  `categories_description` mediumtext,
  `categories_parent` int(11) DEFAULT '-1',
  `categories_total_posts` int(11) DEFAULT '0',
  `categories_total_topics` int(11) DEFAULT '0',
  `categories_last_post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`categories_id`,`categories_name`),
  UNIQUE KEY `id_UNIQUE` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groups_id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_name` varchar(64) NOT NULL,
  `groups_title` varchar(45) NOT NULL,
  PRIMARY KEY (`groups_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `posts_id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_title` varchar(128) DEFAULT NULL,
  `posts_content` longtext,
  `posts_topic_id` int(11) NOT NULL,
  `posts_user_id` int(11) NOT NULL,
  `posts_time` int(11) NOT NULL,
  PRIMARY KEY (`posts_id`,`posts_topic_id`,`posts_user_id`),
  UNIQUE KEY `id_UNIQUE` (`posts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `topics_id` int(11) NOT NULL AUTO_INCREMENT,
  `topics_user_id` int(11) NOT NULL,
  `topics_category` int(11) NOT NULL,
  `topics_time` int(11) DEFAULT NULL,
  `topics_first_post_id` int(11) NOT NULL,
  `topics_last_post_id` int(11) NOT NULL,
  PRIMARY KEY (`topics_id`,`topics_user_id`,`topics_category`,`topics_first_post_id`,`topics_last_post_id`),
  UNIQUE KEY `id_UNIQUE` (`topics_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_name` varchar(45) NOT NULL,
  `users_email` varchar(128) NOT NULL,
  `users_password` varchar(128) DEFAULT NULL,
  `users_level` int(11) DEFAULT '0',
  `users_last_login` int(11) NOT NULL DEFAULT '0',
  `users_groups` varchar(128) NOT NULL DEFAULT '0',
  `users_registered` int(11) NOT NULL,
  `users_activated` int(11) NOT NULL DEFAULT '0',
  `users_avatar_path` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`users_id`,`users_name`),
  UNIQUE KEY `id_UNIQUE` (`users_id`),
  UNIQUE KEY `name_UNIQUE` (`users_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-25 22:10:11
