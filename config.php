<?php

$mysql_config['host'] = "localhost";
$mysql_config['user'] = "root";
$mysql_config['password'] = "";
$mysql_config['database'] = "forum";

$config['enable_total_stats'] = true;
$config['hashsalt'] = 'SALTY';

$mysqli = new mysqli($mysql_config['host'], $mysql_config['user'],
    $mysql_config['password'], $mysql_config['database']);

if (mysqli_connect_errno()) {
    printf("MySQL connection failed: %s", mysqli_connect_error());
    exit();
}
?>
