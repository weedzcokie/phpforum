<?php
include 'common.php';

include 'templates/default.php';
?>
                <form id="login" method="post">
                    <?php
                    if (isset($ERRORS['login'])) {
                        ?>
                    <p class="error">Invalid username/password. Please try again.</p>
                        <?php
                    }
                    ?>
                    <label for="login_username">Username:</label><br>
                    <input type="text" id="login_username" name="username" required>
                    <br>
                    <label for="login_password">Password:</label><br>
                    <input type="password" id="login_password" name="password" required>
                    <br>
                    <input type="checkbox" id="login_remember" name="remember">
                    <label for="login_remember">Remember me</label>
                    <br>
                    <input type="submit" value="Login">
                </form>
<?php
include 'templates/footer.php';
?>
