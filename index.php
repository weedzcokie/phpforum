<?php
include 'common.php';

include 'templates/default.php';

                if (filter_input(INPUT_GET, 'c')) {
                    $category = $mysqli->real_escape_string(filter_input(INPUT_GET, 'c'));
                    $categoryRow = $mysqli->query("SELECT * FROM categories WHERE categories_id = " . $category);
                    $categoryRow = $categoryRow->fetch_array();
                    $parents = get_parent_categories($category);
                    $parents = array_reverse($parents);
                ?>
                <div id="forum-nav">
                    <p><a href="index.php">Home</a><?php
                    foreach ($parents as $parent) {
                        echo ' / <a href="index.php?c='.$parent['id'].'">'.$parent['name'].'</a>';
                    }
                    ?> / <?php echo $categoryRow['categories_name']; ?></p>
                </div>
                <?php
                }
                ?>
                <ul class="announcement">
                    <?php
                    #
                    # TODO: Add category specific announcements
                    #
                $result = $mysqli->query("SELECT *, (SELECT `users_name` FROM users WHERE users_id = announcements.announcements_user_id) AS user_name FROM announcements WHERE announcements_category = -1 AND announcements_display = 1");
                while($row = $result->fetch_array()) {
                ?>
                    <li>
                        <h1><a href="topic.php?announcement=<?php echo $row['announcements_id']; ?>"><?php echo $row['announcements_title'];?></a></h1>
                        <p><?php echo $row['announcements_description'];?></p>
                        <p>Posted by: <strong><a href="users.php?u=<?php echo $row['announcements_user_id'];?>"><?php echo $row['user_name'];?></a></strong> <small><?php echo date("Y-m-d H:i", $row['announcements_time']); ?></small></p>
                    </li>
                <?php
                }
                $result->close();
                ?>
                </ul>
            <?php
                if (isset($_GET['recent'])) {
                    ?>
                    <ul>
                        <p>Recent posts</p>
                    <?php
                    $result = $mysqli->query("SELECT *, (SELECT users_name FROM users WHERE users_id = topics.topics_user_id) AS user_name, (SELECT posts_title from posts WHERE posts_id = topics.topics_first_post_id) AS topic_title,  (SELECT COUNT(posts_id) FROM posts WHERE posts.posts_topic_id = topics.topics_id) AS total_posts, posts.posts_title as post_title, posts.posts_content as post_content, posts.posts_user_id as post_user_id, (SELECT users_name FROM users WHERE users.users_id = post_user_id) as post_username, posts.posts_time as post_time FROM topics LEFT JOIN posts ON posts.posts_id = topics.topics_last_post_id WHERE topics_category != -1 ORDER BY post_time LIMIT 20");
                    while($row = $result->fetch_array()) {
                        ?>
                        <li class="category">
                            <div class="column content">
                                <h2><a href="topic.php?t=<?php echo $row['topics_id']; ?>"><?php echo $row['topic_title']; ?></a></h2>
                                <small>Posted by <strong><a href="users.php?u=<?php echo $row['topics_user_id'];?>"><?php echo $row['user_name']; ?></a></strong> <?php echo date("Y-m-d H:i", $row['topics_time']); ?></small>
                            </div>
                            <div class="column stats">
                                <h4><?php echo ($row['total_posts']-1);?></h4>
                                <small>Replies</small>
                            </div>
                            <div class="column teaser">
                                <a href="topic.php?t=<?php echo $row['topics_id']; ?>&amp;p=<?php echo $row['topics_last_post_id'];?>#<?php echo $row['topics_last_post_id'];?>">
                                    <p><strong><?php echo $row['post_username']; ?></strong> posted <?php echo format_time($row['post_time']); ?></p>
                                    <p class="post-content"><?php echo format_post_preview($row['posts_content']); ?></p>
                                </a>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                    </ul>
                    <?php
                }
                ?>
                <ul id="categories">
                    <?php
                    if (!isset($category)) {
                        $category = -1;
                        echo '<p>Categories</p>';
                    }
                    $result = $mysqli->query("SELECT *, posts.posts_topic_id, posts.posts_user_id, posts.posts_time, posts.posts_content, (SELECT users.users_name FROM users WHERE users.users_id = posts.posts_user_id) AS last_post_username FROM categories LEFT JOIN posts ON posts.posts_id = categories.categories_last_post_id WHERE categories_parent = " . $category);
                    if ($result->num_rows > 0 && $category != -1) {
                        ?>
                        <h3>Subcategories</h3>
                        <?php
                    }
                    while($row = $result->fetch_array()) {
                        $total_topics = $row['categories_total_topics'];
                        $total_posts = $row['categories_total_posts'];
                        ?>
                        <li class="category">
                            <div class="column content">
                                <h1><a href="index.php?c=<?php echo $row['categories_id']; ?>"><?php echo $row['categories_name'];?></a></h1>
                                <small><?php echo $row['categories_description']; ?></small>
                                <?php
                                $result2 = $mysqli->query("SELECT categories_id, categories_name FROM categories WHERE categories_parent = " . $row['categories_id']);
                                if ($result2->num_rows > 0) {
                                ?>
                                <ul id="cat-subforums">
                                    <?php
                                    while ($row2 = $result2->fetch_array()) {
                                        ?>
                                        <li><a href="index.php?c=<?php echo $row2['categories_id'];?>"><?php echo $row2['categories_name']; ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="column stats">
                                <h4><?php echo $total_topics;?></h4>
                                <small>Topics</small>
                            </div>
                            <div class="column stats">
                                <h4><?php echo $total_posts;?></h4>
                                <small>Posts</small>
                            </div>
                            <div class="column teaser">
                                <?php
                                if ($row['categories_last_post_id'] == '') {
                                    ?>
                                    <p>Last post not avalaible</p>
                                    <?php
                                } else {
                                ?>
                                <a href="topic.php?t=<?php echo $row['posts_topic_id']; ?>&amp;p=<?php echo $row['categories_last_post_id'];?>#<?php echo $row['categories_last_post_id'];?>">
                                    <p><strong><?php echo $row['last_post_username']; ?></strong> posted <?php echo format_time($row['posts_time']); ?></p>
                                    <p class="post-content"><?php echo format_post_preview($row['posts_content']); ?></p>
                                </a>
                                <?php
                                }
                                ?>
                            </div>
                        </li>
                        <?php
                    }
                    $result->close();
                    if ($category != -1) {
                        $result = $mysqli->query("SELECT COUNT(topics_id) AS topics_count FROM topics WHERE topics_category = " . $category);
                        $topics_count = $result->fetch_array()[0];
                        $result->close();
                        $page = $mysqli->real_escape_string(filter_input(INPUT_GET,'page',FILTER_VALIDATE_INT));
                        if ($page == '') {
                            $page = 0;
                        }
                        $result = $mysqli->query("SELECT *, (SELECT users_name FROM users WHERE users_id = topics.topics_user_id) AS user_name, (SELECT posts_title from posts WHERE posts_id = topics.topics_first_post_id) AS topic_title,  (SELECT COUNT(posts_id) FROM posts WHERE posts.posts_topic_id = topics.topics_id) AS total_posts, posts.posts_title as post_title, posts.posts_content as post_content, posts.posts_user_id as post_user_id, (SELECT users_name FROM users WHERE users.users_id = post_user_id) as post_username, posts.posts_time as post_time FROM topics LEFT JOIN posts ON posts.posts_id = topics.topics_last_post_id WHERE topics_category = " . $category . " LIMIT " . ($page*10) . ", 10");
                        if ($result->num_rows > 0) {
                            ?>
                    <ul id="topics">
                        <h3>Topics</h3>
                        <?php
                        if (isset($_SESSION['logged_in'])) {
                            ?>
                            <a href="new.php?option=topic&amp;c=<?php echo $category; ?>">New topic</a>
                            <?php
                        }
                            while($row = $result->fetch_array()) {
                                ?>
                            <li class="category">
                                <div class="column content">
                                    <h2><a href="topic.php?t=<?php echo $row['topics_id']; ?>"><?php echo $row['topic_title']; ?></a></h2>
                                    <small>Posted by <strong><a href="users.php?u=<?php echo $row['topics_user_id'];?>"><?php echo $row['user_name']; ?></a></strong> <?php echo date("Y-m-d H:i", $row['topics_time']); ?></small>
                                </div>
                                <div class="column stats">
                                    <h4><?php echo ($row['total_posts']-1);?></h4>
                                    <small>Replies</small>
                                </div>
                                <div class="column teaser">
                                    <a href="topic.php?t=<?php echo $row['topics_id']; ?>&amp;p=<?php echo $row['topics_last_post_id'];?>#<?php echo $row['topics_last_post_id'];?>" title="FORUM NAME > TOPIC">
                                        <p><strong><?php echo $row['post_username']; ?></strong> posted <?php echo format_time($row['post_time']); ?></p>
                                        <p class="post-content"><?php echo format_post_preview($row['post_content']); ?></p>
                                    </a>
                                </div>
                            </li>
                                <?php
                            }
                            ?>
                    </ul>
                        <?php
                        # Page stuff
                        ?>
                        <ul class="pagination">
                            <p>Pages</p>
                        <?php
                        $pages = ceil($topics_count/10);
                        for ($i = 1;$i<=$pages;$i++) {
                            ?>
                            <li><a href="index.php?c=<?php echo $category; ?>&amp;page=<?php echo ($i-1);?>"><?php echo $i; ?></a></li>
                            <?php
                        }
                        ?>
                        </ul>
                            <?php
                        } else if ($page > 0) {
                            ?>
                            <p>There are not that many topics in this category. Go to <a href="index.php?c=<?php echo $category;?>">page 1</a></p>
                            <?php
                        } else {
                            ?>
                    <p>There are no topics in this category yet.</p>
                            <?php
                        }
                    }
                    ?>
                </ul>
<?php
include 'templates/footer.php';
?>
