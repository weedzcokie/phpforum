<?php
include 'common.php';
if (filter_input(INPUT_POST, 'reg_username')) {
    $username = filter_input(INPUT_POST, 'reg_username');
    $password = filter_input(INPUT_POST, 'reg_password');
    $re_password = filter_input(INPUT_POST, 'reg_re_password');
    $email = filter_input(INPUT_POST, 'reg_email');
    $rules = filter_input(INPUT_POST, 'reg_rules');
    if (strlen($username) >= 3 && strlen($password) >= 6 && $password == $re_password && $rules == "on") {
        $username = $mysqli->real_escape_string($username);
        $password = $mysqli->real_escape_string(hash('sha256', $password.$config['hashsalt']));
        $email = $mysqli->real_escape_string($email);
        $result = $mysqli->query("SELECT count(users_id) FROM users WHERE users_name = '" . $username . "'");
        $row = $result->fetch_array();
        $result->close();
        if ($row[0] > 0) {
            $ERRORS['register'] = 1;
        } else {
            $time = $mysqli->real_escape_string(time());
            $mysqli->query("INSERT INTO users (`users_name`,`users_email`,`users_password`,`users_level`,`users_registered`)
                VALUES('".$username."','".$email."','".$password."',0,".$time.")");
            $user['register_success'] = true;
        }
    }
}

include 'templates/default.php';
?>
                <form id="register" method="post">
                    <?php
                    if (isset($ERRORS['register'])) {
                        if ($ERRORS['register'] == 1) {
                            ?>
                    <p class="error">Username is already taken.</p>
                            <?php
                        }
                    }
                    if (isset($user['register_success'])) {
                        ?>
                    <p class="success">Your account was created</p>
                        <?php
                    }
                    ?>
                    <label for="reg_username">Username:</label><br>
                    <input type="text" id="reg_username" name="reg_username" required minlength="3"<?php
                    echo (filter_input(INPUT_POST, 'reg_username') ? ' value="'.filter_input(INPUT_POST, 'reg_username').'"' : ''); ?>>
                    <br>
                    <label for="reg_password">Password:</label><br>
                    <input type="password" id="reg_password" name="reg_password" required minlength="6">
                    <br>
                    <label for="reg_re_password">Confirm password:</label><br>
                    <input type="password" id="reg_re_password" name="reg_re_password" required minlength="6">
                    <br>
                    <label for="reg_email">Email:</label><br>
                    <input type="email" id="reg_email" name="reg_email" required<?php
                    echo (filter_input(INPUT_POST, 'reg_email') ? ' value="'.filter_input(INPUT_POST, 'reg_email').'"' : ''); ?>>
                    <br>
                    <input type="checkbox" id="reg_rules" name="reg_rules" required>
                    <label for="reg_rules">I have read the rules and user agreements</label>
                    <br>
                    <input type="submit" value="Register">
                </form>
<?php
include 'templates/footer.php';
?>
