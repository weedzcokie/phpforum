<?php
$starttime = microtime(true);
session_start();
include 'config.php';

$ERRORS = null;

include 'functions.php';

// Check login status
if (filter_input(INPUT_GET, 'logout')) {
    unset($_SESSION['username']);
    unset($_SESSION['password']);
    unset($_SESSION['logged_in']);
    session_unset();
    header('Location: index.php');
    die();
}
if (filter_input(INPUT_POST, 'username') && filter_input(INPUT_POST, 'password')) {
    $username = filter_input(INPUT_POST, 'username');
    $password = hash('sha256', filter_input(INPUT_POST, 'password').$config['hashsalt']);
} else if (isset($_SESSION['username']) && isset($_SESSION['password'])) {
    $username = $_SESSION['username'];
    $password = $_SESSION['password'];
}
if (isset($username) && isset($password)) {
    $username_esc = $mysqli->real_escape_string($username);
    $password_esc = $mysqli->real_escape_string($password);
    $result = $mysqli->query("SELECT * FROM users WHERE users_name = '" . $username_esc . "' AND users_password = '" . $password_esc . "';");
    if ($result->num_rows == 1) {
        $row = $result->fetch_array();
        $id = $mysqli->real_escape_string($row['users_id']);
        $time = $mysqli->real_escape_string(time());
        $mysqli->query("UPDATE users SET users_last_login = " . $time ." WHERE users_id = " . $id);
        $_SESSION['user_id'] = $id;
        if ($row['users_activated'] == 0) {
            $_SESSION['activated'] = false;
        }
        $_SESSION['username'] = $row['users_name'];
        $_SESSION['password'] = $password;
        $_SESSION['logged_in'] = true;
        $_SESSION['access_level'] = $row['users_level'];
    } else {
        global $ERRORS;
        $ERRORS['login'] = 1;
    }
}

 ?>
