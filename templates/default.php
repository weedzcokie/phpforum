<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Forum</title>
        <link rel="stylesheet" href="style.css" />
        <script src="main.js"></script>
    </head>
    <body>
        <div id="global-container">
            <nav>
                <div id="nav-container">
                    <div id="eoa">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="index.php?recent">Recent</a></li>
                            <li><a href="index.php?hot">Hot topics</a></li>
                            <li><a href="users.php">Users</a></li>
                            <li><a href="groups.php">Groups</a></li>
                        </ul>
                    </div>
                    <div id="user-menu">
                        <?php if (!isset($_SESSION['logged_in'])) { ?>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <?php } else { ?>
                        <ul class="dropdown">
                            <li><a href="users.php?u=<?php echo $_SESSION['user_id'];?>">
                                <?php echo $_SESSION['username']; ?><?php if (isset($_SESSION['activated']) && $_SESSION['activated'] == 0) { ?>
                                <span class="user-warning" title="User not activated!">(!)</span>
                                <?php } ?>
                            </a></li>
                            <li><a href="?logout=true">Logout</a></li>
                        </ul>
                            <?php } ?>
                    </div>
                    <div id="searchbar">
                        <form method="post" id="nav-search" action="search.php">
                            <input type="text" minlength="3" name="searchquerry" required><input type="submit" value="Search">
                        </form>
                    </div>
                </div>
            </nav>
            <div id="main-container">
