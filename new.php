<?php
include 'common.php';

if (filter_input(INPUT_GET, 'option') && filter_input(INPUT_POST, 'post')) {
    $allowed = true;
    $option = filter_input(INPUT_GET, 'option');
    if ($option == 'topic') {
        if ($_SESSION['access_level'] < 1) {
            $allowed = false;
        }

    } else if ($option == 'post') {
        if ($_SESSION['access_level'] < 1) {
            $allowed = false;
        }
        if (filter_input(INPUT_POST, 'post-content')) {
            $escaped_content = $mysqli->real_escape_string(htmlentities(filter_input(INPUT_POST, 'post-content')));
            if (strlen($escaped_content) < 3) {
                $allowed = false;
            }
            $escaped_title = $mysqli->real_escape_string(htmlentities(filter_input(INPUT_POST, 'post-title')));
            $user_id = $mysqli->real_escape_string($_SESSION['user_id']);
        } else {
            $allowed = false;
        }
        #
        #   TODO: make sure user is allowed to post in topic
        #
        if (filter_input(INPUT_GET, 'topic')) {
            $topic_id = $mysqli->real_escape_string(filter_input(INPUT_GET, 'topic'));
        } else {
            $allowed = false;
        }

        if ($allowed) {
            $time = $mysqli->real_escape_string(time());
            $query = "INSERT INTO posts(posts_title, posts_content, posts_topic_id, posts_user_id, posts_time) VALUES('".$escaped_title."','".$escaped_content."',".$topic_id.",".$user_id.",".$time.")";
            #echo $query;
            $mysqli->query($query);
            $result = $mysqli->query("SELECT `AUTO_INCREMENT` FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$mysql_config['database']."' AND TABLE_NAME = 'posts'");
            $row = $result->fetch_array();
            $result->close();
            $mysqli->query("UPDATE topics SET topics_last_post_id = " . ($row[0]-1) . " WHERE topics_id = " . $topic_id);
            header('Location: topic.php?t=' . $topic_id . '&p=' . ($row[0]-1) . '#' . ($row[0]-1));
        } else {
            if (isset($_SERVER['HTTP_REFERER'])) {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            } else {
                header('Location: index.php');
            }
        }

    } else if ($option == 'announcement') {

    } else if ($option == 'category') {

    }
} else if (filter_input(INPUT_GET, 'option')) {

} else {
    header('Location: index.php');
}

include 'templates/default.php';



include 'templates/footer.php';
?>
