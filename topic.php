<?php
include 'common.php';

#
#   TODO: implement bbcode parser and ways to format posts
#

include 'templates/default.php';

if (filter_input(INPUT_GET, 't')) {
    $topic = $mysqli->real_escape_string(filter_input(INPUT_GET, 't'));
    $result = $mysqli->query("SELECT topics.*, posts.posts_title AS topic_title, (SELECT COUNT(posts.posts_id) FROM posts WHERE posts.posts_topic_id = topics.topics_id) AS posts_count FROM topics LEFT JOIN posts ON posts.posts_id = topics.topics_first_post_id WHERE topics_id = " . $topic);
    $topic = $result->fetch_array();
    $result->close();

    if ($topic['posts_count'] > 0) {
        $category = $topic['topics_category'];
        $result = $mysqli->query("SELECT * FROM categories WHERE categories_id = " . $category);
        $row = $result->fetch_array();
        $parents = get_parent_categories($category);
        $parents = array_reverse($parents);
        ?>
    <div id="forum-nav">
        <p><a href="index.php">Home</a><?php
            foreach ($parents as $parent) {
                echo ' / <a href="index.php?c='.$parent['id'].'">'.$parent['name'].'</a>';
            }
            ?> / <a href="index.php?c=<?php echo $row['categories_id']; ?>"><?php echo $row['categories_name']; ?></a> / <?php echo $topic['topic_title']; ?></p>
        </div>
        <ul id="topic">
            <a href="new.php?option=post&amp;topic=<?php echo $topic['topics_id'];?>">New reply</a>
        <?php
        $page = $mysqli->real_escape_string(filter_input(INPUT_GET,'page',FILTER_VALIDATE_INT));
        if ($page == '') {
            $page = 0;
        }
        $result = $mysqli->query("SELECT posts.*, users.users_id, users.users_name, users.users_groups, (SELECT groups.groups_title FROM groups WHERE groups.groups_id = users.users_groups) AS group_title, users.users_activated, users.users_avatar_path FROM posts LEFT JOIN users ON users.users_id = posts.posts_user_id WHERE posts_topic_id = " . $topic['topics_id'] . " ORDER BY posts_time asc LIMIT " . ($page * 10) . ", 10");
        while ($row = $result->fetch_array()) {
            ?>
            <li class="post<?php echo (filter_input(INPUT_GET, 'p') == $row['posts_id'] ? ' highlight' : '');?>">
                <a name="<?php echo $row['posts_id'];?>"></a>
                <div class="post-header">
                    <time datetime=""><?php echo format_time($row['posts_time']);?></time>
                    <span class="float-right"><a href="topic.php?p=<?php echo $row['posts_id'];?>">Link to post</a></span>
                </div>
                <div class="user-column">
                    <p><a href="users.php?u=<?php echo $row['users_id'];?>"><?php echo $row['users_name']; ?></a></p>
                    <p><?php echo $row['group_title']; ?></p>
                    <a href="users.php?u=<?php echo $row['users_id'];?>"><img class="post-user-avatar" src="<?php echo $row['users_avatar_path']; ?>"></a>
                    <!-- additional user info -->
                </div>
                <div class="post-column">
                    <h4><?php echo $row['posts_title']; ?></h4>
                    <p><?php echo format_post($row['posts_content']); ?></p>
                </div>
                <div class="post-footer">
                    <a href="new.php?option=post&amp;topic=<?php echo $topic['topics_id'];?>&amp;quote=<?php echo $row['posts_id'];?>">Quote</a>
                </div>
            </li>
            <?php
        }
        # Page stuff
        ?>
        <ul class="pagination">
            <p>Pages</p>
        <?php
        $pages = ceil($topic['posts_count']/10);
        for ($i = 1;$i<=$pages;$i++) {
            ?>
            <li><a href="topic.php?t=<?php echo $topic['topics_id']; ?>&amp;page=<?php echo ($i-1);?>"><?php echo $i; ?></a></li>
            <?php
        }
        ?>
        </ul>
        <?php
        if (isset($_SESSION['logged_in'])) {
            ?>
            <form id="quick-reply" method="post" action="new.php?option=post&amp;topic=<?php echo $topic['topics_id'];?>">
                <label for="form-post-content">Quick reply</label><br>
                <textarea id="form-post-content" name="post-content" required></textarea><br>
                <input type="submit" name="post" value="Submit">
                <input type="submit" name="preview" value="Preview">
            </form>
        </ul>
            <?php
        }
    }
} else if (filter_input(INPUT_GET, 'p')) {
    $post = $mysqli->real_escape_string(filter_input(INPUT_GET, 'p'));
    $result = $mysqli->query("SELECT posts.*, topics.topics_category, topics.topics_id, (SELECT posts.posts_title FROM posts WHERE posts.posts_id = topics.topics_first_post_id) AS topics_name,(SELECT categories.categories_name FROM categories WHERE categories.categories_id = topics.topics_category) AS category_name, users.users_id, users.users_name, users.users_groups, (SELECT groups.groups_title FROM groups WHERE groups.groups_id = users.users_groups) AS group_title, users.users_activated, users.users_avatar_path FROM posts LEFT JOIN users ON users.users_id = posts.posts_user_id LEFT JOIN topics ON topics.topics_id = posts.posts_topic_id WHERE posts_id = " . $post . " ORDER BY posts_time asc");
    $post = $result->fetch_array();
    $parents = get_parent_categories($post['topics_category']);
    $parents = array_reverse($parents);
    ?>
    <div id="forum-nav">
        <p><a href="index.php">Home</a><?php
        foreach ($parents as $parent) {
            echo ' / <a href="index.php?c='.$parent['id'].'">'.$parent['name'].'</a>';
        }
        ?> / <a href="index.php?c=<?php echo $post['topics_category']; ?>"><?php
            echo $post['category_name']; ?></a> / <a href="topic.php?t=<?php
            echo $post['posts_topic_id']; ?>&amp;p=<?php
            echo $post['posts_id']; ?>#<?php
            echo $post['posts_id'];?>"><?php
            echo $post['topics_name']; ?></a> > Viewing post</p>
    </div>
    <div class="post">
        <a name="<?php echo $post['posts_id'];?>"></a>
        <div class="post-header">
            <time datetime=""><?php echo format_time($post['posts_time']);?></time>
            <span class="float-right"><a href="topic.php?p=<?php echo $post['posts_id'];?>">Link to post</a></span>
        </div>
        <div class="user-column">
            <p><a href="users.php?u=<?php echo $post['users_id'];?>"><?php echo $post['users_name']; ?></a></p>
            <p><?php echo $post['group_title']; ?></p>
            <a href="users.php?u=<?php echo $post['users_id'];?>"><img class="post-user-avatar" src="<?php echo $post['users_avatar_path']; ?>"></a>
            <!-- additional user info -->
        </div>
        <div class="post-column">
            <h4><?php echo $post['posts_title']; ?></h4>
            <p><?php echo format_post($post['posts_content']); ?></p>
        </div>
        <div class="post-footer">
            <a href="new.php?option=post&amp;topic=<?php echo $post['topics_id'];?>&amp;quote=<?php echo $post['posts_id'];?>">Quote</a>
        </div>
    </div>
    <?php
}

include 'templates/footer.php';
?>
